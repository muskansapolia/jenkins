package org.example.Factory;
import org.example.model.Employee;

import java.util.Collection;

public interface EmployeeDashboard {

    public Employee createEmployee(Employee employee);
    public Collection<Employee> getAllEmployees();
}
